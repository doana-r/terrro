
test_that("errors are thrown when input are incorrects", {
  expect_error(calculerCAU(donnee = CAU_tot_ex,
                           mafor = "BS",
                           methode = "truc"), "methode")

  expect_error(calculerCAU(donnee = CAU_tot_ex,
                           mafor = "truc",
                           methode = "cumule"), "mafor")


  expect_error(calculerCAU(donnee = CAU_tot_ex,
                           mafor = "BS",
                           methode = "cumule",
                           court = "a"), "court")

  expect_error(calculerCAU(mafor = "truc",
                           methode = "cumule"), "donnee")

})


test_that("calculerCAU gives the expected data and graphical outputs", {
  expect_type(calculerCAU(
    donnee = CAU_tot_ex,
    mafor = "BS",
    methode = "cumule"
  ),
  "list")

  expect_length(calculerCAU(
    donnee = CAU_tot_ex,
    mafor = "BS",
    methode = "cumule"
  ),
  3)


  if (requireNamespace("vdiffr", quietly = TRUE)) {
    vdiffr::expect_doppelganger("BS-cumule-plot", calculerCAU(
      donnee = CAU_tot_ex,
      mafor = "BS",
      methode = "cumule")$graphique
    )
    # run when new figure vdiffr::manage_cases()

  }





})
