test_that("two data.frames have the same structure", {

  expect_true(mtcars[1:5, ] %==% mtcars[6:10, ])

  expect_error(mtcars %==% LETTERS, "data.frames")

  expect_false(mtcars[1:10] %==% mtcars[1:9])
  expect_message(mtcars[1:10] %==% mtcars[1:9], "columns")

  mtcars2 <- setNames(mtcars, LETTERS[seq_along(mtcars)])
  expect_false(mtcars %==% mtcars2)
  expect_message(mtcars %==% mtcars2, "names")

  mtcars3 <- as.data.frame(lapply(mtcars, factor))
  expect_false(mtcars %==% mtcars3)
  expect_message(mtcars %==% mtcars3, "types")

})
