
<!-- README.md is generated from README.Rmd. Please edit that file -->

# terrro <img src='man/figures/logo.png' align="right" height="138.5" />

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
[![codecov](https://codecov.io/gl/doana-r/terrro/branch/%5Cx6d6173746572/graph/badge.svg?token=C6N3B76059)](https://codecov.io/gl/doana-r/terrro)
[![pipeline
status](https://gitlab.com/doana-r/terrro/badges/master/pipeline.svg)](https://gitlab.com/doana-r/terrro/-/commits/master)
<!-- badges: end -->

Voici une boîte à outils de fonctions de R créée dans le cadre de
l’analyses de données du projet TERO, qui a pour objectif d’acquérir
des références agronomiques pour l’utilisation de Mafor (matières
fertilisantes organiques) comme fertilisant de la canne à sucre en
substitution complète ou partielle des engrais minéraux importés à La
Réunion.

## Installation

Installez la dernière version de terrro qui est sur
[gitlab](https://gitlab.com/doana-r/terrro) avec les commandes :

``` r
library(remotes)

install_git("https://gitlab.com/doana-r/terrro")
```

## Example

Voici en exemple ce que l’on peut faire avec ce package :

``` r
library(terrro)
#> Hi Amélie! I'm your friend :)
```

1.  Créer un jeu de données parfait pour pouvoir calculer les
    coefficients apparent d’utilisation (CAU) des mafor, à partir des
    data.frames de la base de données propre.

<!-- end list -->

``` r
CAU_tot_ex <- creerCAU_tot(
  recolte = recolte_ex,
  suivi = suivi_ex,
  info = info_ex,
  uree = uree_ex
)
summary(CAU_tot_ex)
#>             Id_mod        Id_PE            Id_chp    Id_bloc  Id_cycle  
#>  0 Xsp - 0,9Xsp: 36   P01    :  5   CFPPA     :162   B1:176   C1R0:163  
#>  Ecum1         : 36   P03    :  5   EST       : 62   B2:176   C1R1:151  
#>  Ecum2         : 36   P04    :  5   P13_LaMare:270   B3:178   C1R2:108  
#>  Ecum3         : 36   P06    :  5   SUD       : 36            C1R3: 54  
#>  LP1           : 33   P07    :  5                             C1R4: 54  
#>  LP2           : 27   P08    :  5                                       
#>  (Other)       :326   (Other):500                                       
#>      rdt.MF           rdt.MS          n_abs           N.app.ha     
#>  Min.   : 58.69   Min.   :19.95   Min.   : 45.23   Min.   :   0.0  
#>  1st Qu.:122.23   1st Qu.:37.71   1st Qu.:104.83   1st Qu.: 109.1  
#>  Median :153.01   Median :45.30   Median :134.27   Median : 195.9  
#>  Mean   :156.88   Mean   :45.53   Mean   :137.84   Mean   : 281.6  
#>  3rd Qu.:190.94   3rd Qu.:53.14   3rd Qu.:165.39   3rd Qu.: 346.3  
#>  Max.   :268.38   Max.   :76.23   Max.   :347.72   Max.   :1120.4  
#>                                                    NA's   :135     
#>       type         mafor        modalite      freq.apport         calcul   
#>  mafor  :494   Ecum   :108   Min.   :1.000   Min.   :1.000   cumule  :138  
#>  mineral: 36   BS     : 72   1st Qu.:1.000   1st Qu.:1.000   direct  :224  
#>                CDV    : 72   Median :2.000   Median :2.000   indirect:132  
#>                CP     : 72   Mean   :1.911   Mean   :1.802   zero    : 36  
#>                LV     : 72   3rd Qu.:3.000   3rd Qu.:2.000                 
#>                (Other): 98   Max.   :3.000   Max.   :3.000                 
#>                NA's   : 36   NA's   :36      NA's   :36                    
#>  dernier.N.app   
#>  Min.   :   0.0  
#>  1st Qu.: 124.3  
#>  Median : 217.4  
#>  Mean   : 302.6  
#>  3rd Qu.: 405.9  
#>  Max.   :1120.4  
#>  NA's   :3
```

2.  Calculer les CAU et les visualiser graphiquement pour une mafor avec
    une méthode donnée.

<!-- end list -->

``` r
BS_cumule <- calculerCAU(donnee = CAU_tot_ex, mafor = "BS", methode = "cumule")
BS_cycle <- calculerCAU(donnee = CAU_tot_ex, mafor = "BS", methode = "cycle")

# parfois c'est sympa de mettre 2 graphiques ensemble :
ggpubr::ggarrange(BS_cycle$graphique,
  BS_cumule$graphique,
  ncol = 2,
  legend = "top"
)
```

<img src="man/figures/README-graphique-1.png" width="100%" style="display: block; margin: auto;" />

> **Subtilité** pour LP et FPG, comme les moda sont 1 ou 2 (pas le 3),
> il faut mettre methode = “cycle”. Il n’y aura que le calcul direct
> bien sûr.

3.  Récupérer un tableau contenant les paramètres des droites (intercept
    et pente = CAU) ainsi que la significativité des tests de leur
    égalité à 0.

<!-- end list -->

``` r
BS_cumule$coefficients
#>    Id_cycle     Id_chp coef_ordonnee coef_pente pval_ordonnee pval_pente
#> 1      C1R0      CFPPA     198.16960 0.16972010  3.788741e-11 0.40462233
#> 2      C1R1      CFPPA     183.61195 0.09522218  2.846297e-10 0.61009704
#> 3      C1R2      CFPPA     145.05698 0.26617265  7.239613e-08 0.18819147
#> 4      C1R0        EST     118.26362         NA  3.680912e-06         NA
#> 5      C1R1        EST     111.88118         NA  9.314922e-06         NA
#> 6      C1R0 P13_LaMare     101.23307 0.10726266  4.296317e-05 0.33890459
#> 7      C1R1 P13_LaMare      89.20628 0.14318275  2.310324e-04 0.17563264
#> 8      C1R2 P13_LaMare     110.04029 0.17073868  1.215861e-05 0.11244493
#> 9      C1R3 P13_LaMare      87.28520 0.17254949  3.003966e-04 0.09333156
#> 10     C1R4 P13_LaMare      87.63513 0.19289155  2.864094e-04 0.05715265
#> 11     C1R0        SUD     117.93057         NA  3.864234e-06         NA
#> 12     C1R1        SUD     151.77938         NA  2.712061e-08         NA
BS_cycle$coefficients
#>    Id_cycle     Id_chp coef_ordonnee   coef_pente pval_ordonnee pval_pente
#> 1      C1R0      CFPPA     199.37246 -0.263061637  1.665133e-14 0.19724982
#> 2      C1R1      CFPPA     172.71715 -0.274470253  3.295723e-12 0.17876233
#> 3      C1R2      CFPPA     140.59461 -0.005208003  2.107149e-09 0.94833031
#> 4      C1R0        EST     118.26362           NA  1.255680e-06         NA
#> 5      C1R1        EST     111.88118           NA  3.741246e-06         NA
#> 6      C1R0 P13_LaMare      99.77738  0.103677416  5.886813e-06 0.35420079
#> 7      C1R1 P13_LaMare      80.78564  0.050456438  1.665158e-04 0.65123561
#> 8      C1R2 P13_LaMare     102.30452  0.119051721  3.624071e-06 0.17277248
#> 9      C1R3 P13_LaMare      79.79445  0.119599696  1.938781e-04 0.17083272
#> 10     C1R4 P13_LaMare      78.17170  0.152989419  2.536209e-04 0.01399427
#> 11     C1R0        SUD     117.93057           NA  1.329959e-06         NA
#> 12     C1R1        SUD     151.77938           NA  3.168751e-09         NA
```

-----

*Work in progress…*
